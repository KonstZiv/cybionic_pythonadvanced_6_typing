# CyBionic_PythonAdvanced_6_typing

Материалы к лекции "Типизированный Python" курса PythonAdvanced от CyberBionic. Рассматриваем возможности и использование модуля typing на примере.

Построено на разборе и некоторой переработке материалов [статьи](https://realpython.com/python-type-checking/)